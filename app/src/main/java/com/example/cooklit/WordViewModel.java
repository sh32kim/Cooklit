package com.example.cooklit;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

public class WordViewModel extends AndroidViewModel {
    private WordRepository mRepository;
    private LiveData<List<com.example.cooklit.Word>> mAllWords;

    public WordViewModel (Application application){
        super(application);
        mRepository = new WordRepository(application);
        mAllWords = mRepository.getAllWords();
    }

    LiveData<List<com.example.cooklit.Word>> getAllWords() {return mAllWords;}

    public void insert(com.example.cooklit.Word word) { mRepository.insert(word);}

}
